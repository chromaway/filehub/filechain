## Filechain deployment

Filechains have a dependecy to a [Filehub](https://gitlab.com/chromaway/filehub/filehub).

A Filechain must have a trusted Filehub configured, and a Filehub can be trusted by multiple Filechains . In order to deploy and run Filechain a Filehub BRID must be configured in as module args _chromia.yml_.

```yaml
blockchains:
  filechain:
    module: main
    moduleArgs:
      fs:
        trusted_filehub_brid: x"0AFB9C36BF12ED2FA00BDC30F4A8A855261F0FF0473142CF31B2A1809AC1C968"
    config:
      gtx:
        modules:
          - "net.postchain.d1.iccf.IccfGTXModule"
```

For local development and testing [deploy-bundle](https://gitlab.com/chromaway/filehub/deploy-bundle) should be used.
